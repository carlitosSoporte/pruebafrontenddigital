import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MainMenuComponent } from './componentes/main-menu/main-menu.component';
import { MenuCarpoolingComponent } from './componentes/menu-carpooling/menu-carpooling.component';
import { MenuUserComponent } from './componentes/menu-user/menu-user.component';
@NgModule({
  declarations: [
    AppComponent,
    MainMenuComponent,
    MenuCarpoolingComponent,
    MenuUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
