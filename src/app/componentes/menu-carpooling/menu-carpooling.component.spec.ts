import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuCarpoolingComponent } from './menu-carpooling.component';

describe('MenuCarpoolingComponent', () => {
  let component: MenuCarpoolingComponent;
  let fixture: ComponentFixture<MenuCarpoolingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuCarpoolingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuCarpoolingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
