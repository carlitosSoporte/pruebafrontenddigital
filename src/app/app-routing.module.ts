import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainMenuComponent } from './componentes/main-menu/main-menu.component';
import { MenuUserComponent } from './componentes/menu-user/menu-user.component';
import { MenuCarpoolingComponent } from './componentes/menu-carpooling/menu-carpooling.component';

const routes: Routes = [
  {path: 'menu', component: MainMenuComponent, pathMatch: 'full'},
  {path: 'userMenu', component: MenuUserComponent, pathMatch: 'full'},
  {path: 'carpoolingMenu', component: MenuCarpoolingComponent, pathMatch: 'full'},
  {path: '', redirectTo: '/menu', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
